# -*- coding: utf-8 -*-

# User prefix. Describes the cpmpany.
USER_PREFIX = '7b'

# Project settings

PROJECT_NAME = 'aboutevents'

MEDIA_APP = PROJECT_NAME + '_media'
STATIC_APP = PROJECT_NAME + '_static'
VIRTUALENV_NAME = 'env_' + PROJECT_NAME
APP_USERNAME = USER_PREFIX + PROJECT_NAME
COPY_COMMANDS = []

# Settings for different servers
SERVERS = {
    'staging': {
        'KEY_FILENAME': '~/.ssh/id_dsa',
        'USER': 'drom',
        'HOST': '94.75.245.143',
        'BRANCH': 'develop',
        'DOMAIN': 'the7bits.com',
        'SUBDOMAINS': ['aboutevents'],
        'USE_MAIN_DOMAIN': False,
        'DEBUG': False,
        'APACHE_PORT': 27696,
        'MACHINE_NAME': 'Web210',
        'PROJECT_NAME': PROJECT_NAME,
        'VIRTUALENV_NAME': VIRTUALENV_NAME,
        'APP_USERNAME': APP_USERNAME,
        'MEDIA_APP': MEDIA_APP,
        'STATIC_APP': STATIC_APP,
        'DB_NAME': 'drom_'+PROJECT_NAME,
        'MAILBOX_NAME': 'drom_'+PROJECT_NAME,
        'SUPPORT_EMAIL': 'aboutevents@the7bits.com',
        'UPGRADE_EMAIL': 'aboutevents-upgrade@the7bits.com',
        'COPY_COMMANDS': COPY_COMMANDS,
    },
    'production': {
        'KEY_FILENAME': '~/.ssh/id_dsa',
        'USER': 'sbits',
        'HOST': '95.211.171.72',
        'BRANCH': 'master',
        'DOMAIN': 'aboutevents.net',
        'SUBDOMAINS': ['www'],
        'USE_MAIN_DOMAIN': True,
        'DEBUG': False,
        'APACHE_PORT': 21045,
        'MACHINE_NAME': 'Web332',
        'PROJECT_NAME': PROJECT_NAME,
        'VIRTUALENV_NAME': VIRTUALENV_NAME,
        'APP_USERNAME': APP_USERNAME,
        'MEDIA_APP': MEDIA_APP,
        'STATIC_APP': STATIC_APP,
        'DB_NAME': 'sbits_'+PROJECT_NAME,
        'MAILBOX_NAME': 'sbits_'+PROJECT_NAME,
        'SUPPORT_EMAIL': 'support@aboutevents.net',
        'UPGRADE_EMAIL': 'aboutevents-upgrade@aboutevents.net',
        'COPY_COMMANDS': COPY_COMMANDS,
        },
}

# List of application for launching migration every update.
MY_APPS = [
    'eventsapp',
]

# Remote repository for downloading sources from.
REPOSITORY = 'ssh://git@bitbucket.org/the7bits/aboutevents.git'

# In this version only mysql is supported. Don't touch this.
DB_TYPE = 'mysql'

# Set this in True if you want to see SSH logging
SSH_LOGGING = False

# This template using for adding virtual host if you use the main domain for site access
HTTPD_TEMPLATE_DOMAIN = '''
<VirtualHost *:%(apache_port)d>
  WSGIScriptAlias / /home/%(username)s/webapps/django/%(project_name)s/%(project_name)s/wsgi.py
  ServerName %(domain)s
  ServerAlias %(subdomain)s.%(domain)s
  ErrorLog "logs/%(project_name)s_errors.log"
</VirtualHost>
'''

# This template using for adding virtual host if you use the subdomain for site access
HTTPD_TEMPLATE_SUBDOMAIN = '''
<VirtualHost *:%(apache_port)d>
  WSGIScriptAlias / /home/%(username)s/webapps/django/%(project_name)s/%(project_name)s/wsgi.py
  ServerName %(subdomain)s.%(domain)s
  ErrorLog "logs/%(project_name)s_errors.log"
</VirtualHost>
'''



