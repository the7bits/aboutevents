#!/usr/bin/env python
import os
import sys

#sys.path = ['/home/korkholeh/webapps/django/garantilla', '/home/korkholeh/webapps/django', '/home/korkholeh/webapps/django/lib/python2.5'] + sys.path
if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "aboutevents.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
