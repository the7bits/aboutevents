# coding: utf-8
from datetime import date, timedelta
from django import template
import datetime


register = template.Library()

@register.filter
def diff_date(event):
    value = event.date_start
    value_end = event.date_end
    now = datetime.date.today()
    difference = value - now
    if (now-value).days>=0 and (value_end - now).days>=0:
        return u'Сегодня'
    if (now-value).days>=0:
            return u'Прошло: ' + value.strftime("%d.%m.%Y") 
    if difference.days>1:
        str = u'Осталось: ' + unicode(difference.days) + u' дней'
        return str
    if difference.days==1:
        return u'Завтра'
    if difference.days==0:
        return u'Сегодня'

@register.filter
def keys (first,second):
    if isinstance(first,list):
        return first+[second]
    else:
        return [first,second]

@register.filter
def date_ge(date,value):
    if date:
        if value<=date:
            return True
        else:
            return False
    else:
        return False

@register.filter
def date_le(date,value):
    if date:
        if value>=date:
            return True
        else:
            return False
    else:
        return False

@register.filter
def elem(list,i):
    return list[i]

@register.filter
def day(i):
    if (isinstance(i,date)):
        return i.day
    else:
        return ''

@register.filter
def add(i):
    i[1]+=1
    return ''
