# coding: utf-8
from django.db import models
from django.contrib.auth.models import User
from PIL import Image
from django.db.models.signals import post_save,pre_save,pre_delete
import random
import string
import pytils
from datetime import datetime
from utils import *

def make_upload_poster_path(instance, filename):
    now = datetime.now().strftime('%Y/%m/%d/%H/%M/%S')
    ext = '.'+filename.split('.')[1]
    name = pytils.translit.slugify(filename)[:-len(ext)+1]+ext
    return u"upload/posters/%s/%s" % (now, name)

def make_upload_logo_path(instance, filename):
    now = datetime.now().strftime('%Y/%m/%d/%H/%M/%S')
    ext = '.'+filename.split('.')[1]
    name = pytils.translit.slugify(filename)[:-len(ext)+1]+ext
    return u"upload/logos/%s/%s" % (now, name)


class ServerSetting(models.Model):
    name = models.CharField(max_length=128, verbose_name=u'Опция',primary_key=True)
    value = models.BooleanField(verbose_name=u'Значение')

    def __unicode__(self):
        return self.name


class Country(models.Model):
    code = models.CharField(max_length=5)
    name = models.CharField(max_length=128)

    def __unicode__(self):
        return self.name


class Town(models.Model):
    name = models.CharField(max_length=128)
    country = models.ForeignKey(Country)

    def __unicode__(self):
        return self.country.name + " " + self.name


class Tag(models.Model):
    name = models.CharField(max_length=128,verbose_name=u'Название тега')

    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name = u'Тег'
        verbose_name_plural = u'Теги'


class Event(models.Model):
    name = models.CharField(max_length=128,verbose_name=u'Название')
    date_start = models.DateField(verbose_name=u'Дата начала')
    date_end = models.DateField(verbose_name=u'Дата окончания')
    date_create = models.DateField(auto_now_add=True,verbose_name=u'Дата создания')
    description = models.TextField(verbose_name=u'Описание',default="")
    user = models.ForeignKey(User,verbose_name=u'Создатель')
    slug = models.SlugField(verbose_name=u'Слаг',null=True,blank=True,max_length=300)
    poster = models.ImageField(upload_to=make_upload_poster_path,verbose_name=u'Постер',null=True,blank=True)
    logo = models.ImageField(upload_to=make_upload_logo_path,verbose_name=u'Логотип',null=True,blank=True)
    tags = models.ManyToManyField(Tag,blank=True,null=True,verbose_name=u'Теги')
    accepted = models.BooleanField(default=False, verbose_name=u'Подверждена модератором')
    location = models.ForeignKey(Town, blank=True, null=True)

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return  '/events/'+self.slug + '.html'
    class Meta:
        verbose_name = u'Событие'
        verbose_name_plural = u'События'

class Review(models.Model):
    name = models.CharField(max_length=128,verbose_name=u'Название')
    date_create = models.DateField(verbose_name=u'Дата создания')
    text = models.TextField(verbose_name=u'Текст')
    user = models.ForeignKey(User,verbose_name=u'Создатель')
    event = models.ForeignKey(Event,verbose_name=u'Событие')
    photo = models.ImageField(upload_to='upload/photos/%Y/%m/%d/%H/%M/%S/',blank=True,null=True,verbose_name=u'Фото')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'Рецензия'
        verbose_name_plural = u'Рецензии'


class UserProfile(models.Model):
    user = models.OneToOneField(User,verbose_name=u'Пользователь',null=True,blank=True)
    name = models.CharField(max_length=128,verbose_name=u'Имя',null=True,blank=True,default="")
    address = models.CharField(max_length=256,verbose_name=u'Адресс',null=True,blank=True,default="")
    phone = models.CharField(max_length=18,verbose_name=u'Телефон',null=True,blank=True,default="")
    description = models.TextField(verbose_name=u'Описание',null=True,blank=True,default="")
    photo_1 = models.ImageField(upload_to='upload/photos/%Y/%m/%d/%H/%M/%S/',blank=True,null=True,verbose_name=u'Фото')
    photo_2 = models.ImageField(upload_to='upload/photos/%Y/%m/%d/%H/%M/%S/',blank=True,null=True,verbose_name=u'Фото')
    photo_3 = models.ImageField(upload_to='upload/photos/%Y/%m/%d/%H/%M/%S/',blank=True,null=True,verbose_name=u'Фото')
    photo_4 = models.ImageField(upload_to='upload/photos/%Y/%m/%d/%H/%M/%S/',blank=True,null=True,verbose_name=u'Фото')
    photo_5 = models.ImageField(upload_to='upload/photos/%Y/%m/%d/%H/%M/%S/',blank=True,null=True,verbose_name=u'Фото')
    can_post = models.ManyToManyField('self', blank=True, null=True,symmetrical=False, related_name='journalists' )
    accepted = models.BooleanField(default=False, verbose_name=u'Подвержден модератором')
    moder = models.BooleanField(default=False, verbose_name=u'Пользователь является модератором')
    reviewer = models.BooleanField(default=False, verbose_name=u'Пользователь является рецензентом')
    moderate_user_events = models.BooleanField(default=True, verbose_name=u'Модерировать события пользователя')

    def thumb1(self):
        return get_thumbnail_url(self.photo_1.url,size=128)
    def thumb2(self):
        return get_thumbnail_url(self.photo_2.url,size=128)
    def thumb3(self):
        return get_thumbnail_url(self.photo_3.url,size=128)
    def thumb4(self):
        return get_thumbnail_url(self.photo_4.url,size=128)
    def thumb5(self):
        return get_thumbnail_url(self.photo_5.url,size=128)

    thumb1.short_description = 'thumb_photo1'
    thumb1.allow_tags = True
    thumb2.short_description = 'thumb_photo2'
    thumb2.allow_tags = True
    thumb3.short_description = 'thumb_photo3'
    thumb3.allow_tags = True
    thumb4.short_description = 'thumb_photo4'
    thumb4.allow_tags = True
    thumb5.short_description = 'thumb_photo5'
    thumb5.allow_tags = True
    def __unicode__(self):
        if self.name :
            return self.name
        else: return "None"

    class Meta:
        verbose_name = u'Профиль пользователя'
        verbose_name_plural = u'Профили пользователя'

def post_save_handler(sender, **kwargs):
    if kwargs['instance'].photo_1:create_thumbnail(kwargs['instance'].photo_1.path,size=128)
    if kwargs['instance'].photo_2:create_thumbnail(kwargs['instance'].photo_2.path,size=128)
    if kwargs['instance'].photo_3:create_thumbnail(kwargs['instance'].photo_3.path,size=128)
    if kwargs['instance'].photo_4:create_thumbnail(kwargs['instance'].photo_4.path,size=128)
    if kwargs['instance'].photo_5:create_thumbnail(kwargs['instance'].photo_5.path,size=128)


def pre_delete_handler(sender, **kwargs):
    if kwargs['instance'].photo_1:delete_thumbnail(kwargs['instance'].photo_1.path,size=128)
    if kwargs['instance'].photo_2:delete_thumbnail(kwargs['instance'].photo_2.path,size=128)
    if kwargs['instance'].photo_3:delete_thumbnail(kwargs['instance'].photo_3.path,size=128)
    if kwargs['instance'].photo_4:delete_thumbnail(kwargs['instance'].photo_4.path,size=128)
    if kwargs['instance'].photo_5:delete_thumbnail(kwargs['instance'].photo_5.path,size=128)


post_save.connect(post_save_handler, sender = UserProfile)
pre_delete.connect(pre_delete_handler, sender = UserProfile)

def fix_slug(sender, instance, raw, **kwargs):
    if isinstance(instance,Event):
        if not instance.slug or not instance.slug.strip():
            instance.slug = '_'.join((pytils.translit.slugify(instance.name),instance.date_start.strftime('%Y-%m-%d'),instance.date_end.strftime('%Y-%m-%d'),''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(6))))

def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)

def fix_image_width(sender, instance, created, **kwargs):
    if isinstance(instance,Event):
        im = instance.poster
        if im:
            filename = str(im.path).decode('utf-8')
            image = Image.open(filename)
            pw = im.width
            ph = im.height
            if pw>760:
                koef = 760.0/float(pw)
                ph *= koef
                image = image.resize((760,int(ph)), Image.ANTIALIAS)
                image.save(filename)

def fix_photo_width(sender, instance, created, **kwargs):
    if isinstance(instance,UserProfile):
        im = instance.photo_1
        if im:
            filename = str(im.path)
            image = Image.open(filename)
            pw = im.width
            ph = im.height
            if pw>760:
                koef = 760.0/float(pw)
                ph *= koef
                image = image.resize((760,int(ph)), Image.ANTIALIAS)
                image.save(filename)
        im = instance.photo_2
        if im:
            filename = str(im.path)
            image = Image.open(filename)
            pw = im.width
            ph = im.height
            if pw>760:
                koef = 760.0/float(pw)
                ph *= koef
                image = image.resize((760,int(ph)), Image.ANTIALIAS)
                image.save(filename)
        im = instance.photo_3
        if im:
            filename = str(im.path)
            image = Image.open(filename)
            pw = im.width
            ph = im.height
            if pw>760:
                koef = 760.0/float(pw)
                ph *= koef
                image = image.resize((760,int(ph)), Image.ANTIALIAS)
                image.save(filename)
        im = instance.photo_4
        if im:
            filename = str(im.path)
            image = Image.open(filename)
            pw = im.width
            ph = im.height
            if pw>760:
                koef = 760.0/float(pw)
                ph *= koef
                image = image.resize((760,int(ph)), Image.ANTIALIAS)
                image.save(filename)
        im = instance.photo_5
        if im:
            filename = str(im.path)
            image = Image.open(filename)
            pw = im.width
            ph = im.height
            if pw>760:
                koef = 760.0/float(pw)
                ph *= koef
                image = image.resize((760,int(ph)), Image.ANTIALIAS)
                image.save(filename)


def user_activate(sender, instance, created, **kwargs):
    if isinstance(instance, UserProfile):
        if instance.accepted:
            user = instance.user
            user.is_active = instance.accepted
            user.save()

post_save.connect(user_activate, sender=UserProfile)
post_save.connect(create_user_profile, sender=User)
pre_save.connect(fix_slug, sender=Event)
post_save.connect(fix_image_width, sender=Event)
post_save.connect(fix_photo_width, sender=UserProfile)

