# coding: utf-8
from django.contrib.auth.models import User
from datetime import *
__author__ = 'lex'
from django import forms
from eventsapp.models import Event,UserProfile,Review

class EventForm(forms.ModelForm):
    tags = forms.CharField(label=u'Теги')
    author = forms.IntegerField(widget=forms.HiddenInput(),required=False)
    def clean(self):
        if self.data['date_start'] and self.data['date_end']:
            if datetime.strptime(self.data['date_start'].replace('.','/')+" 00:00:00","%d/%m/%Y %H:%M:%S").date()>datetime.strptime(self.data['date_end'].replace('.','/')+" 00:00:00","%d/%m/%Y %H:%M:%S").date():
                raise forms.ValidationError(u"Даты не сходятся")
        return super(EventForm,self).clean()
    class Meta(object):
        model = Event
        exclude = ('user','slug','date_create', 'tags', 'location', 'accepted')


class RegisterForm(forms.ModelForm):
    password1 = forms.CharField(
        label=u'Пароль еще раз',
        required=True,
        widget=forms.PasswordInput()
    )

    def clean(self):
        try:
            User.objects.get(username = self.data['username'])
            raise forms.ValidationError(u"Пользователь с таким логином уже зарегистрирован")
        except User.DoesNotExist:
            pass
        if self.data['password']!=self.data['password1']:
            raise forms.ValidationError(u"Пароли не совпадают")
        return super(RegisterForm,self).clean()

    class Meta:
        model = User
        fields =('username','password','email')
        widgets = {
            'password': forms.PasswordInput(),
            }
    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)

        for key in self.fields:
            self.fields[key].required = True




class EditUserForm(forms.ModelForm):


    class Meta(object):
        model = UserProfile
        exclude = ('user', 'accepted', 'moder', 'can_post')


class ReviewForm(forms.ModelForm):
    author = forms.IntegerField(widget=forms.HiddenInput())
    class Meta(object):
        model = Review
        exclude = ('user','event','date_create')


