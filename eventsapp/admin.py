# coding: utf-8
__author__ = 'lex'
from django.contrib import admin
from eventsapp.models import Event, UserProfile, Tag, ServerSetting,Review
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
from django import forms
from django.shortcuts import render_to_response
from django.template import RequestContext


class ServerSettingAdmin(admin.ModelAdmin):
    list_display = ('name', 'value')
    list_filter = ('name', 'value')

class UserChangeForm(forms.Form):
    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)
    user = forms.ModelChoiceField(queryset=User.objects.all())

class EventAdmin(admin.ModelAdmin):
    list_display = ('date_start', 'date_end', 'date_create',
                    'description', 'user', 'name', 'slug', 'accepted')
    list_filter = ('date_start', 'date_end', 'date_create', 'user',
                   'name', 'slug', 'accepted')
    actions = ['change_user_action',]
    search_fields = ['name','description',]


    def change_user_action(self, request, queryset):
        if 'do_action' in request.POST:
            form = UserChangeForm(request.POST)
            if form.is_valid():
                user = form.cleaned_data['user']
                for c in queryset:
                    c.user = User.objects.get(username=user)
                    c.save()
                return
        else:
            form = UserChangeForm()
        return render_to_response('admin/aboutevents/change_user.html', {
            'title': u'Выберите пользователя',
            'objects': queryset,
            'form': form},
            context_instance = RequestContext(request))
    change_user_action.short_description = u'Поменять пользователя'

class ReviewAdmin(admin.ModelAdmin):
    list_display = ('name', 'text', 'user','event')
    list_filter = ('name', 'text', 'user','event')


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('name', 'address', 'phone')
    list_filter = ('name', 'address', 'phone')


class TagAdmin(admin.ModelAdmin):
    list_display = ('name',)

admin.site.register(Event, EventAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Review, ReviewAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(ServerSetting, ServerSettingAdmin)

admin.site.unregister(User)

class UserProfileInline(admin.StackedInline):
    model = UserProfile
    extra = 0
    max_num = 1
    can_delete = False

class UserProfileAdmin(UserAdmin):
    inlines = (UserProfileInline,)

admin.site.register(User, UserProfileAdmin)





