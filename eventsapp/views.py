# coding: utf-8
import random
import string
import calendar
from django.http import Http404
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.db.models.query_utils import Q
from django.db.models import F
from django.db.models import Count
from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.views.generic import ListView, DetailView, CreateView
from django.views.generic.base import TemplateView
from django.views.generic.edit import UpdateView
from eventsapp.models import *
from eventsapp.forms import *
import datetime,pytils
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.utils.decorators import method_decorator
from django.contrib.syndication.views import Feed

class EventsView(ListView):
    model = Event
    paginate_by = 50
    context_object_name = 'events'


    def dispatch(self, request, *args, **kwargs):
        now = datetime.datetime.now()
        self.now_day = now.strftime("%d")
        self.now_month = now.strftime("%m")
        self.now_year = now.strftime("%Y")
        return super(EventsView,self).dispatch( request, *args, **kwargs)

    def get_queryset(self):
        return Event.objects.filter(
            Q(date_end__gte = "%s-%s-%s" % (self.now_year,self.now_month,self.now_day)) & Q(accepted = True)
        ).extra(select={
            'before':'date_start<="%s"' % (datetime.datetime.now().strftime('%Y-%m-%d'),)
        }).order_by('date_start')


class MainView(EventsView):
    template_name = 'eventsapp/event_list.html'


class UsersEventView(EventsView):

    def dispatch(self, request, *args, **kwargs):
        self.id = kwargs['id']
        try:
            return super(UsersEventView,self).dispatch( request, *args, **kwargs)
        except:
            raise Http404


    def get_queryset(self):
        return super(UsersEventView,self).get_queryset().filter(user__id = self.id)

    def get_context_data(self, **kwargs):
        context = super(UsersEventView,self).get_context_data(**kwargs)
        context['username'] = User.objects.get(pk = self.id)
        return context


class TagEventView(EventsView):

    def dispatch(self, request, *args, **kwargs):
        self.tag = kwargs.get('tag',False)
        return super(TagEventView,self).dispatch( request, *args, **kwargs)

    def get_queryset(self):
        try:
            tag = Tag.objects.get(name = self.tag.lower())
        except:
            raise Http404
        return tag.event_set.filter(
            date_end__gte = "%s-%s-%s" % (self.now_year,self.now_month,self.now_day)
        ).extra(select={
            'before':'date_start<="%s"' % (datetime.datetime.now().strftime('%Y-%m-%d'),)
        }).order_by('date_start')

    def get_context_data(self, **kwargs):
        context = super(TagEventView,self).get_context_data(**kwargs)
        context['tagname'] = self.tag
        return context


class DateEventView(EventsView):

    def dispatch(self, request, *args, **kwargs):
        self.year = kwargs['year']
        self.month = kwargs['month']
        self.day = kwargs['day']
        try:
            return super(DateEventView,self).dispatch( request, *args, **kwargs)
        except:
            raise Http404



    def get_queryset(self):

        return Event.objects.filter(
            Q(date_end__gte = "%s-%s-%s" % (self.year,self.month,self.day)) &
            Q(date_start__lte = "%s-%s-%s" % (self.year,self.month,self.day))
        ).order_by('date_start')



class CreateEvent(CreateView):
    model = Event
    form_class = EventForm
    template_name = 'eventsapp/create_event.html'

    def dispatch(self, request, *args, **kwargs):
        return super(CreateEvent,self).dispatch( request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CreateEvent, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            context['avaliable_authors'] = self.request.user.get_profile().can_post.all()
        else:
            context['avaliable_authors'] = [User.objects.get(username='AboutEvents').get_profile(),]
        return context

    def form_valid(self, form):
        instance = form.save(commit=False)
        create_without_registration = False
        if not self.request.user.is_authenticated():
            instance.user = User.objects.get(username='no_name')
            create_without_registration = True
        else:
            instance.user = UserProfile.objects.get(pk=int(form.cleaned_data['author'])).user
        instance.save()
        tag_list =[]
        for tag in form.cleaned_data['tags'].split(','):
            if tag.lower().strip():
                m_tag,created = Tag.objects.get_or_create(name = tag.lower().strip())
                if created:m_tag.save()
                tag_list.append(m_tag)
        instance.tags.clear()
        instance.tags.add(*tag_list)
        if create_without_registration:
            instance.accepted = False
        else:
            instance.accepted = self.request.user.is_active and (self.request.user.get_profile().moder or self.request.user.get_profile().moder or (not self.request.user.get_profile().moderate_user_events) or  (not ServerSetting.objects.get(pk='moderate_events').value))
        instance.save()
        return HttpResponseRedirect(reverse('detail-event-view', args = [instance.slug,]))


class EventView(DetailView):
    model = Event
    context_object_name = 'event'
    slug_field  = 'slug'

    def get_context_data(self, **kwargs):
        context = super(EventView, self).get_context_data(**kwargs)
        context['description']=' '.join(context['event'].description.replace('\r\n','')[:150].split(' ')[:-1])
        context['reviews'] = Review.objects.filter(event = context['event'])
        context['can_edit'] = context['event'].user.get_profile().can_post.all()
        return context


class EventUpdate(UpdateView):
    model = Event
    form_class = EventForm
    template_name = 'eventsapp/edit_event.html'

    @method_decorator(login_required())
    def dispatch(self, request, *args, **kwargs):
        return super(EventUpdate,self).dispatch( request, *args, **kwargs)
        
    def get_form_kwargs(self):
        kwargs = super(EventUpdate, self).get_form_kwargs()
        kwargs.update({
            'initial': {'tags':', '.join([x.name for x in self.get_object().tags.all()])}
        })
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(EventUpdate, self).get_context_data(**kwargs)
        context['avaliable_authors'] = self.request.user.get_profile().can_post.all()
        return context


    def form_valid(self, form):
        instance = form.save(commit=False)
        if self.request.user.get_profile().moder or instance.user == self.request.user or instance.user in self.request.user.get_profile().can_post.all():
            instance.user = User.objects.get(pk=int(form.cleaned_data['author']))
            instance.slug = pytils.translit.slugify('_'.join((instance.name,instance.date_start.strftime('%Y-%m-%d'),instance.date_end.strftime('%Y-%m-%d'))))+''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(6))
            instance.save()
            tag_list = []
            for tag in form.cleaned_data['tags'].split(','):
                if tag.lower().strip()!='':
                    m_tag,created = Tag.objects.get_or_create(name = tag.lower().strip())
                    if created:m_tag.save()
                    tag_list.append(m_tag)
            instance.tags.clear()
            instance.tags.add(*tag_list)
            instance.save()
            return HttpResponseRedirect(reverse('detail-event-view', args = [instance.slug,]))
        else:
            return HttpResponseForbidden()


class RegisterView(CreateView):
    form_class = RegisterForm
    model = User
    template_name = 'eventsapp/register_form.html'
    success_url = '/events/register_success/'
    def form_valid(self, form):
        user =  form.save(commit = True)
        user.set_password(form.cleaned_data['password'])
        user.save()
        if not ServerSetting.objects.get(pk = 'moderate_user').value:
            user.is_active = True
            profile = user.get_profile()
            profile.accepted = True
            profile.save()
        else:
            profile = user.get_profile()
            profile.accepted = False
            profile.save()
            return HttpResponseRedirect('/events/wait_activation/')
        user = authenticate(username = form.cleaned_data['username'],password = form.cleaned_data['password'])
        login(self.request,user)
        return HttpResponseRedirect('/events/register_success/')



class EditProfileView(UpdateView):
    form_class = EditUserForm
    model = UserProfile
    template_name = 'eventsapp/edit_profile.html'

    @method_decorator(login_required())
    def dispatch(self, request, *args, **kwargs):
        return super(EditProfileView,self).dispatch( request, *args, **kwargs)

    def form_valid(self, form):
        instance = form.save(commit = False)
        if instance.user != self.request.user and not self.request.user.get_profile().moder:
            return HttpResponseForbidden()
        instance.save()
        return HttpResponseRedirect(reverse('users-event-list', args=[instance.user.id,"1"]))


class ActivateUser(TemplateView):

    @method_decorator(login_required())
    def dispatch(self, request, *args, **kwargs):
        return super(ActivateUser,self).dispatch( request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        if request.user.get_profile().moder:
            id = int(kwargs['id'])
            user = User.objects.get(pk = id)
            profile = user.get_profile()
            profile.can_post = [profile, ]
            profile.accepted = True
            profile.save()
            return HttpResponseRedirect('/events/moder/')
        else:
            return HttpResponseForbidden()


class ActivateEvent(TemplateView):

    @method_decorator(login_required())
    def dispatch(self, request, *args, **kwargs):
        return super(ActivateEvent,self).dispatch( request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        if request.user.get_profile().moder:
            id = int(kwargs['id'])
            event = Event.objects.get(pk = id)
            event.accepted = True
            event.save()
            return HttpResponseRedirect('/events/moder/')
        else:
            return HttpResponseForbidden()


class ModerView(TemplateView):
    template_name = 'eventsapp/moder_view.html'

    @method_decorator(login_required())
    def dispatch(self, request, *args, **kwargs):
        if request.user.get_profile().moder:
            return super(ModerView,self).dispatch( request, *args, **kwargs)
        else:
            return HttpResponseForbidden()

    def get_context_data(self, **kwargs):
        context = super(ModerView, self).get_context_data(**kwargs)
        context['users'] = UserProfile.objects.filter(accepted = False)
        context['events'] = Event.objects.filter(accepted = False)
        return context


class AdminView(TemplateView):
    template_name = 'eventsapp/admin_view.html'

    @method_decorator(login_required())
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_superuser:
            return super(AdminView,self).dispatch( request, *args, **kwargs)
        else:
            return HttpResponseForbidden()

    def get_context_data(self, **kwargs):
        context = super(AdminView, self).get_context_data(**kwargs)
        context['users'] = User.objects.annotate(events_number=Count('event'))
        return context




class DeleteUser(TemplateView):

    @method_decorator(login_required())
    def dispatch(self, request, *args, **kwargs):
        return super(DeleteUser,self).dispatch( request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        if request.user.get_profile().moder:
            id = int(kwargs['id'])
            user = User.objects.get(pk = id)
            user.delete()
            return responseRedirect('/events/moder/')
        else:
            return HttpResponseForbidden()


class DeleteEvent(TemplateView):

    @method_decorator(login_required())
    def dispatch(self, request, *args, **kwargs):
        return super(DeleteEvent,self).dispatch( request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        if request.user.get_profile().moder:
            id = int(kwargs['id'])
            event = Event.objects.get(pk = id)
            event.delete()
            return HttpResponseRedirect('/events/moder/')
        else:
            return HttpResponseForbidden()


class LatestEntriesFeed(Feed):
    title = "Aboutevents.net site news"
    link = "/"
    description = "Updates on changes and additions to Aboutevents.net."

    def items(self):
        return Event.objects.all()

    def item_title(self, item):
        return item.name

    def item_description(self, item):
        return item.description


class CreateReview(CreateView):
    model = Review
    form_class = ReviewForm
    template_name = 'eventsapp/add_review.html'

    @method_decorator(login_required())
    def dispatch(self, request, *args, **kwargs):
        self.event_id =  int(kwargs['id'])
        return super(CreateReview,self).dispatch( request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CreateReview, self).get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.user = User.objects.get(pk=int(form.cleaned_data['author']))
        instance.event = Event.objects.get(pk = self.event_id)
        instance.date_create = datetime.date.today()
        instance.save()
        return HttpResponseRedirect(reverse('detail-event-view', args = [instance.event.slug,]))

class CalendarView(ListView):
    template_name = 'eventsapp/calendar.html'
    context_object_name = 'events'
    model = Event
    month =  int(datetime.date.today().month)
    year =  int(datetime.date.today().year)

    def dispatch(self, request, *args, **kwargs):
        self.month =  kwargs['month']
        self.year =  kwargs['year']
        return super(CalendarView,self).dispatch( request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CalendarView, self).get_context_data(**kwargs)
        context['year']=int(self.year)
        context['month']=int(self.month)
        prev_month=int(self.month)-1
        prev_year=int(self.year)
        if prev_month==0:
            prev_month=12
            prev_year-=1
        context['prev_month']=prev_month
        context['prev_year']=prev_year
        next_month=int(self.month)+1
        next_year=int(self.year)
        if next_month==13:
            next_month=1
            next_year+=1
        context['next_month']=next_month
        context['next_year']=next_year
        months = ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь']
        context['month_name']=months[int(self.month)-1]
        days_matrix=[]
        for i in calendar.monthcalendar(int(self.year),int(self.month)):
            days=[]
            for j in i:
                if j:
                    days.append([datetime.date(int(self.year),int(self.month),j),0])
                else:
                    days.append([0,0])
            days_matrix.append(days)
        context['days_matrix']=days_matrix
        return context

    def get_queryset(self):
        return Event.objects.filter(
            Q(date_end__gte = "%s-%s-%s" % (self.year,self.month,1)) &
            Q(date_start__lte = "%s-%s-%s" % (self.year,self.month,calendar.monthrange(int(self.year),int(self.month))[1]))
        ).order_by('date_start')


class CalendarViewNow(ListView):
    template_name = 'eventsapp/calendar.html'
    context_object_name = 'events'
    model = Event
    month =  int(datetime.date.today().month)
    year =  int(datetime.date.today().year)


    def dispatch(self, request, *args, **kwargs):
        return super(CalendarViewNow,self).dispatch( request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CalendarViewNow, self).get_context_data(**kwargs)
        context['year']=int(self.year)
        context['month']=int(self.month)
        prev_month=int(self.month)-1
        prev_year=int(self.year)
        if prev_month==0:
            prev_month=12
            prev_year-=1
        context['prev_month']=prev_month
        context['prev_year']=prev_year
        next_month=int(self.month)+1
        next_year=int(self.year)
        if next_month==13:
            next_month=1
            next_year+=1
        context['next_month']=next_month
        context['next_year']=next_year
        months = [u'Январь',u'Февраль',u'Март',u'Апрель',u'Май',u'Июнь',u'Июль',u'Август',u'Сентябрь',u'Октябрь',u'Ноябрь',u'Декабрь']
        context['month_name']=months[int(self.month)-1]
        days_matrix=[]
        for i in calendar.monthcalendar(int(self.year),int(self.month)):
            days=[]
            for j in i:
                if j:
                    days.append([datetime.date(int(self.year),int(self.month),j),0])
                else:
                    days.append([0,0])
            days_matrix.append(days)
        context['days_matrix']=days_matrix
        return context

    def get_queryset(self):
        return Event.objects.filter(
            Q(date_end__gte = "%s-%s-%s" % (self.year,self.month,1)) &
            Q(date_start__lte = "%s-%s-%s" % (self.year,self.month,calendar.monthrange(int(self.year),int(self.month))[1]))
        ).order_by('date_start')

    