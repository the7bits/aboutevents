from django.views.generic.base import TemplateView

__author__ = 'lex'
from django.conf.urls.defaults import patterns, include, url
from eventsapp.views import *
from eventsapp.sitemap import EventSitemap

urlpatterns = patterns('',
    url(r'^register_success/$',TemplateView.as_view(template_name='eventsapp/login_succes.html')),
    url(r'^wait_activation/$',TemplateView.as_view(template_name='eventsapp/wait_activation.html')),
    url(r'^page(?P<page>\d+)/$', MainView.as_view(),name='event-list'),
    url(r'^calendar/(?P<day>\d+)/(?P<month>\d+)/(?P<year>\d+)$', DateEventView.as_view(),name='calendar-event-list'),
    url(r'^calendar/(?P<month>\d+)/(?P<year>\d+)/$', CalendarView.as_view(),name='calendar-view'),
    url(r'^calendar/$', CalendarViewNow.as_view(),name='calendar-view-now'),
    url(r'^byuser/(?P<id>\d+)/page(?P<page>\d+)/', UsersEventView.as_view(),name='users-event-list'),
    url(r'^tag/(?P<tag>[^/]+)/page(?P<page>\d+)/', TagEventView.as_view(),name='tag-event-list'),
    url(r'^create/$',CreateEvent.as_view(),name='create-view'),
    url(r'^edit/(?P<pk>\d+)/$', EventUpdate.as_view(),name='edit-event-view'),
    url(r'^register/$',RegisterView.as_view(),name='register'),
    url(r'^activate_event/(?P<id>\d+)/$',ActivateEvent.as_view(),name='activate-event'),
    url(r'^activate_user/(?P<id>\d+)/$',ActivateUser.as_view(),name='activate-user'),
    url(r'^delete_event/(?P<id>\d+)/$',DeleteEvent.as_view(),name='delete-event'),
    url(r'^delete_user/(?P<id>\d+)/$',DeleteUser.as_view(),name='delete-user'),
    url(r'^moder/$',ModerView.as_view(),name='moder-view'),
    url(r'^admin_statistic/$',AdminView.as_view(),name='admin-view'),
    url(r'^profileedit/(?P<pk>\d+)/$', EditProfileView.as_view(),name='profile-edit'),
    url(r'^(?P<slug>[^/]+)\.html$',EventView.as_view(),name='detail-event-view'),
    url(r'^reviews/(?P<id>\d+)/$',CreateReview.as_view(),name='add-review'),
)
