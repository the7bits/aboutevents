from django.contrib.sitemaps import Sitemap
from eventsapp.models import Event

class EventSitemap(Sitemap):
    changefreq = "never"
    priority = 0.5

    def items(self):
        return Event.objects.all()
