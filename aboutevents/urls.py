from django.conf.urls.defaults import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from django.conf import settings
from django.views.generic  import RedirectView
from eventsapp.views import MainView,TagEventView,LatestEntriesFeed

admin.autodiscover()
from eventsapp.sitemap import EventSitemap

sitemaps = {'main':EventSitemap}

urlpatterns = patterns('',
    url(r'^robots.txt$', include('robots.urls')), 
    url(r'^$', MainView.as_view(),kwargs={'page':1}),
    url(r'^favicon\.ico$', RedirectView.as_view(url='/static/favicon.ico')),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^events/', include('eventsapp.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login',{'template_name': 'eventsapp/login.html'},name='login'),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout',{'next_page': '/','template_name': 'eventsapp/logout.html'},name='logout'),
    url(r'^feed/$', LatestEntriesFeed()),
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),
    
)
urlpatterns += patterns('django.contrib.staticfiles.views',url(r'^static/(?P<path>.*)$', 'serve'),)
urlpatterns += patterns('',
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT,
        }),)

urlpatterns+=staticfiles_urlpatterns()
urlpatterns += patterns('',
    url(r'^(?P<tag>[^/]+)/', TagEventView.as_view(),kwargs={'page':1}),
)
